@extends('layouts.site')

<body>
  @section('header')
    <a class="btn btn-primary" href="/">Home</a>
    <a class="btn btn-primary" href="/about">About</a>
    <a class="btn btn-primary" href="/cards">Cards</a>
  @stop

  @section('content')
  <div class="row">
    <div class="card col-md-4">
      <div class="title col-md-8">{{ $card->title }}</div>
        <div class="row">
          <div class="col-md-8">Name: {{ $card->name }}</div>
        </div>
        <div class="row">
          <div class="col-md-8">Email: {{ $card->email }}</div>
        </div>
        <div class="row">
          <div class="col-md-8">Phone: {{ $card->phone }}</div>
        </div>

        @foreach($card->notes as $note)
          <ul>
            <li class="content">Note {{ $note->id }} : {{ $note->body }}
            </li>
          </ul>
        @endforeach
    </div>
  </div>
  @stop
</body>
