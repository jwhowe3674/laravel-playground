@extends('layouts.site')

<body>
  @section('header')
    <a class="btn btn-primary" href="/">Home</a>
    <a class="btn btn-primary" href="/about">About</a>
  @stop

  @section('content')
    <h1 class="title">All Cards</h1>    

    <div class="row">
    @foreach ($cards as $card)
      <div class="col-md-2">
      {{ Form::open(array('url' => $card->path())) }}
        <button type="submit" class="btn btn-primary">
            Card {{ $card->id }} : {{ $card->title }}
        </button>
      {{ Form::close() }}
      </div>
    @endforeach
    </div>
  @stop
</body>
