<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="/assets/css/playground.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div>
		<div id="header" class="container-fluid">
			<div class="row">
				<div id="viewtitle" class="col-md-3">Laravel Cards App</div>
			</div>
			@yield('header')
		</div>

		<div id="body" class="container-fluid">
			@yield('content')
		</div>

		<div id="footer" class="container-fluid">
			<div class="row">
				@yield('footer')
				<div class="col-md-12 copyright">
					Copyright &copy; 2016 Justin W. Howe
				</div>
			</div>
		</div>
	</div>
</body>
</html>
