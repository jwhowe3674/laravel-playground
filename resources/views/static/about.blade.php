@extends('layouts.site')

<!DOCTYPE html>
<html>
    <head>
        <title>Laravel Playground Suite</title>
    </head>

    @section('header')
      <a class="btn btn-primary" href="/">Home</a>
    @stop

    @section('content')
      <div class="title">About Me</div>
      <div class="content">
        Name: Justin W. Howe<br>
        Email: jwhowe2007@gmail.com<br>
        Occupation: Software Engineer
      </div>
      <div class="title">About This App</div>
      <div class="content">
        This is a <strong>rudimentary</strong> app that demonstrates multiple types of features included with a base installation of Laravel 5.2.
      </div>
    @stop
</html>
