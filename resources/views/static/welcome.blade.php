@extends('layouts.site')

<!DOCTYPE html>
<html>
    <head>
        <title>Laravel Playground Suite</title>
    </head>

    @section('header')
      <a class="btn btn-primary" href="/about">About</a>
      <a class="btn btn-primary" href="/cards">Cards</a>
    @stop

    @section('content')
      <div class="title">Laravel 5</div>
      <div class="content">
        <strong>The Howe Family:</strong>
          @foreach ($people as $person)
            <div id="row"><strong>{{ $person }}</strong></div>
          @endforeach
      </div>
      <div class="row">
        <div class="col-md-6 content blackground">
          And in that battle there was a man with more blood on his hands than any other. A man who would commit a crime that would silence the universe. And that man was me. Great men are forged in fire. It is the privilege of lesser men to light the flame. Whatever the cost. Go and be the Doctor that I could never be. Are you capable of speaking without flapping your hands about? I don't have a future. Are you capable of speaking without flapping your hands about? Shall we ask for a better quality of door so we can escape?
        </div>
      </div>
    @stop
</html>
