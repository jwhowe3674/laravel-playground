<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    // Set this up for soft deletes using a trait
    use SoftDeletes;

    // Allow for mass updates
    protected $fillable=['body'];

    // Allow for soft deletes
    protected $dates = ['deleted_at'];

    // This belongs to the Card Model
    public function card()
    {
      return $this->belongsTo(Card::class);
    }
}
