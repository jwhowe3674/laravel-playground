<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Note;

class Card extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'name', 'phone', 'email'];
    protected $dates = ['deleted_at'];

    public function notes()
    {
      return $this->hasMany(Note::class);
    }

    public function path() {
      return '/cards/' . $this->id;
    }  
}
