<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function index()
    {
      $people = ['Justin', 'Mandy', 'Shannon', 'Katherine'];

      return view('static.welcome')->with(compact('people'));
    }

    public function about()
    {
      return view('static.about');
    }
}
