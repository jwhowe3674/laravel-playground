<?php

namespace App\Http\Controllers;

use App\Card;
use App\Http\Requests;
use Illuminate\Http\Request;

class CardsController extends Controller
{
    public function index()
    {
        $cards = Card::All();

        return view('cards.index')->with(compact('cards'));
    }

    public function show(Card $card)
    {
        return view('cards.show')->with(compact('card'));
    }

    public function edit()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }
}
